import pytactx
import random
import time
import math

from main import StateMachineAgent

#COEFFS constant to calculate difficulty
COEFFS = [0, 3, 1, 2]
#
# voisins = {'Jusdeliens Soldier I':
#     {
#         'x': 3,
#         'y': 4,
#         'dir': 3,
#         'ammo': 100,
#         'd': 0,
#         'life': 100,
#         'fire': False,
#         'led': [0, 255, 0],
#         'profile': 0,
#         'team': 0,
#         'dest': '',
#         'eta': [],
#         'chat': ''
#     },
#     'Jusdeliens Soldier II':
#         {
#             'x': 5,
#             'y': 4,
#             'dir': 1,
#             'ammo': 10,
#             'd': 0,
#             'life': 20,
#             'fire': False,
#             'led': [0, 255, 0],
#             'profile': 0,
#             'team': 0,
#             'dest': '',
#             'eta': [],
#             'chat': ''
#         }
# }


def eval(agent, voisins):
    """
    Evaluate the neighborhood of our Agent to determine the easiest enemy to kill.

    :param agent: Our Pytactx Agent !
    :param voisins: The neighborhood of the Agent (all the other agents on the board)
    :return:
    """
    enemy_list = []
    for enemy in enemy_descriptor(agent, voisins):
        enemy_difficulty = enemy_difficulty_calculator(enemy)
        enemy = {'name': enemy['name'], 'difficulty': enemy_difficulty}
        enemy_list.append(enemy)
    target = select_target(enemy_list)
    return target


def enemy_descriptor(agent, voisins):
    """
    Create a list of enemies, simplified to get specific attributes for further treatment.

    :param agent: Our Pytactx Agent !
    :param voisins: The neighborhood of the Agent (all the other agents on the board)
    :return: a list of dict, each dict containing all the useful attributes we want
    """
    for _enemy, enemy_attr in voisins.items():
        ecart_x = abs(agent.x - enemy_attr['x'])
        ecart_y = abs(agent.y - enemy_attr['y'])
        # Evaluate distance between agent and enemy
        enemy_distance = math.sqrt(ecart_x * ecart_x + ecart_y * ecart_y)
        enemy_ammo = int(enemy_attr['ammo'])
        enemy_life = int(enemy_attr['life'])
        enemy = {'name': _enemy, 'life': enemy_life, 'ammo': enemy_ammo, 'distance': enemy_distance}
        yield enemy


def enemy_difficulty_calculator(enemy):
    """
    Calculate the difficulty of an enemy depending on the life, the ammo and the distance
     between the agent and the enemy.

    :param enemy: a dict containing all the attributes of an enemy
    :return: enemy_difficulty an int representing the difficulty to kill this enemy
    """
    value = 0
    for _ in enemy.items():
        value = (enemy['life'] * COEFFS[1]) + (enemy['ammo'] * COEFFS[2]) + (enemy['distance'] * COEFFS[3])
    enemy_difficulty = int(value / sum(COEFFS))
    return enemy_difficulty


def select_target(enemy_list):
    """
    Select the easiest enemy to kill in a list of enemies.

    :param enemy_list: a list of dict, each dict containing the name(str) and the difficulty(int) of one enemy
    :return: target a dict containing the name(str) and the difficulty(int) of the easiest enemy to kill
    """
    min_diff = 100
    target = {}
    for _enemy in enemy_list:
        enemy_difficulty = _enemy['difficulty']
        if enemy_difficulty < min_diff:
            min_diff = enemy_difficulty
            target = _enemy
    return target


if __name__ == '__main__':
    agent = StateMachineAgent("Orel")
    while agent.vie > 0:
        eval(agent, agent.voisins)
        agent.actualiser()
