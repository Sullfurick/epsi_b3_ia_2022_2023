from abc import abstractmethod
import pytactx
import fsm
from enum import Enum
import random
import decision_making


class StateEnum(Enum):
    """ An Enum is a classy way to put a name on a value"""
    PATROL = 0  # "PatrolState"
    FOLLOW = 1  # "FollowState"
    INVESTIGATE = 2  # "InvestigateState"


class BaseState(fsm.State):
    def __init__(self, agent, color):
        self._agent = agent
        self.__color = color

    def handle(self):
        ...  # ADD common behaviour to execute whatever the state
        r, g, b = self.__color
        self._agent.changerCouleur(r, g, b)
        # self._agent.tirer((self._agent.distance != 0))
        self.onHandle()

    @abstractmethod
    def onHandle(self):
        ...


class PatrolState(BaseState):
    """
    Specific State that should send signals to StateMachine
    Implement handle() behavior
    """

    def __init__(self, agent):
        super().__init__(agent, (0, 255, 0))

    def onHandle(self):
        # if agent detected
        if self._agent.distance != 0:
            # switch to FollowState
            self.switch_state(StateEnum.FOLLOW.value)  # en python, self est obligatoire
        # otherwise
        else:
            # Move agent around a patrol point
            self._agent.deplacer(self._agent.x + random.randint(-1, 1),
                                 self._agent.y + random.randint(-1, 1))
            self._agent.orienter((self._agent.orientation + 1) % 4)


class FollowState(BaseState):
    def __init__(self, agent):
        super().__init__(agent, (255, 0, 0))
        self.__xEnnemy = 0
        self.__yEnnemy = 0

    def onHandle(self):
        # If agent on sight, save location
        if (self._agent.distance != 0):
            if self._agent.orientation == 0:
                self.__xEnnemy = self._agent.x + self._agent.distance
                self.__yEnnemy = self._agent.y
            elif self._agent.orientation == 1:
                self.__xEnnemy = self._agent.x
                self.__yEnnemy = self._agent.y - self._agent.distance
            elif self._agent.orientation == 2:
                self.__xEnnemy = self._agent.x - self._agent.distance
                self.__yEnnemy = self._agent.y
            elif self._agent.orientation == 3:
                self.__xEnnemy = self._agent.x
                self.__yEnnemy = self._agent.y + self._agent.distance
        # If not arrived at the last known location of the ennemy
        # isArrived = ( self.x == self.__xEnnemy and self.y == self.__yEnnemy )
        # isNotArrived = not(self.x == self.__xEnnemy and self.y = self.__yEnnemy)
        # isNotArrived = (self.x != self.__xEnnemy or self.y != self.__yEnnemy)
        if not (self._agent.x == self.__xEnnemy and self._agent.y == self.__yEnnemy):
            self._agent.deplacer(self.__xEnnemy, self.__yEnnemy)
        else:  # Enemy not on site anymore
            # Investigate
            self.switch_state(StateEnum.INVESTIGATE.value)


class InvestigateState(BaseState):
    def __init__(self, agent):
        super().__init__(agent, (255, 128, 0))

    def onHandle(self):
        """
            An Agent walks around in the research area
        """
        # if agent not dead
        if agent.distance != 0:
            # fire
            agent.tirer(True)
            ...
        # otherwise
        else:
            # Move agent around a patrol point
            self.switch_state(StateEnum.PATROL.value)


class StateMachineAgent(pytactx.Agent):
    def __init__(self, myId):
        # create state machine
        self.__state_machine = fsm.StateMachine()  # register all states depending on pytactx context
        # it uses append(), the order matter
        self.__state_machine.add_state(PatrolState(self))
        self.__state_machine.add_state(FollowState(self))
        self.__state_machine.add_state(InvestigateState(self))
        # set the initial state depending on pytactx context
        self.__state_machine.set_actual_state(StateEnum.PATROL.value)
        # finally the super init
        _pwd = input('pass ? ')
        super().__init__(id=myId,
                         username="demo",
                         password=_pwd,
                         arena="demo",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)

    def quandActualiser(self):
        self.__state_machine.handle()


if __name__ == '__main__':
    agent = StateMachineAgent("Orel'")
    while agent.vie > 0:
        decision_making.eval(agent, agent.voisins)
        print(decision_making.eval(agent, agent.voisins))
        agent.actualiser()
