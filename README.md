# Pytactx
## StateMachine & Decision making

Ce repository contient le rendu pour le cours IA de Julien Arné @jusdeliens.

L'objectif était de développer une machine à état ainsi qu'un algorithme qui permet
de décider quel ennemi nous devons attaquer en premier dans le jeu Pytactx 
développé par Julien (essayez-le c'est très amusant).

Vous retrouverez dans ce readme le diagramme d'état ainsi que le diagramme de séquences
correspondant à ce projet.

Le diagramme de classe est manquant car le temps alloué à la réalisation de
ce projet était limité.

### Pytactx : https://jusdeliens.com/pytactx/
Vous aurez besoin d'identifiants pour accéder aux arènes publiques de pytactx.
Vous pouvez les obtenir directement sur le site de jusdeliens !

Pour run le projet :
```bash
git clone https://gitlab.com/Sullfurick/epsi_b3_ia_2022_2023.git
cd epsi_b3_ia_2022_2023
python src/main.py
```

# Diagrammes

#### Diagramme d'état

```mermaid
classDiagram
    class IState
    IState : *handle()

    class State
    State o-- StateMachine
    IState <|-- State
    State : -StateMachine context
    State : +setContext(context)
    State : +switch_state(state)

    class BaseState
    State <|-- BaseState
    State : #agent
    State : -color
    State : +handle()
    State : *onHandle()


    class PatrolState
    BaseState <|-- PatrolState
    PatrolState : +onHandle()

    class FollowState
    BaseState <|-- FollowState
    FollowState : -xEnemy
    FollowState : -yEnemy
    FollowState : +onHandle()


    class InvestigateState
    BaseState <|-- InvestigateState
    InvestigateState : +onHandle()

    class StateMachine
    StateMachine *-- State
    StateMachine : -actualState
    StateMachine : -states
    StateMachine : +add_state(state)
    StateMachine : +set_actual_state(state)
    StateMachine : +handle()


    class Agent
    Agent : +x
    Agent : +y
    Agent : +distance
    Agent : +orientation
    Agent : +tirer(trigger)
    Agent : +changerCouleur(r ,g ,b)
    Agent : +actualiser()
    Agent : +deplacer(x, y)
    Agent : +orienter(direction)
    Agent : *quandActualiser()
    Agent *-- StateMachine

    class StateMachineAgent
    Agent <|-- StateMachineAgent
    StateMachineAgent : +quandActualiser() 
    
```

#### Diagramme de séquence

```mermaid
sequenceDiagram
    participant A as Agent
    participant P as PatrolState
    participant F as FollowState
    participant I as InvestigateState

    A->>P : Spawn
    P->>P : Patrol
    P->>F: Enemy detected
    loop two rounds
        F->>I: Go to last enemy position
    end
    loop until enemy is dead
    I->>I: Fire
    end
    A->>I: Move to last enemy position
    I->>F: Enemy lost (nobody is here)
    F->>P: Back to PatrolState
    P->>A: Routine

```